# Anketa FIT - asociační pravidla

Domácí úkol č. 2 předmětu MI-DDW.

Na základě dat z Ankety FIT za poslední 2 semestry byla vygenerována asociační pravidla - tato aplikace umožňuje jejich prohlížení a třídění.

## Použité technologie

- RapidMiner
- Web crawler (Chrome extension)
- Pandas (Python)
- Lumen (PHP framework)
