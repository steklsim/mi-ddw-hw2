<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
	private $conclusions = [
		0 => 'count_voted = range1 [-∞ - 13]',
		1 => 'percent_voted = range1 [-∞ - 25.750]',
		2 => 'count_finished = range1 [-∞ - 55.200]',
		3 => 'count_enrolled = range1 [-∞ - 87.400]',
		4 => 'rating_helpful = range1 [-∞ - 1.500]',
		5 => 'rating_topic = range1 [-∞ - 1.800]',
		6 => 'rating_difficulty = range3 [2.600 - 3.400]',
		7 => 'percent_finished = range4 [79.250 - ∞]',
		8 => 18102,
		9 => 'semester = b142',
		10 => 'rating_specifications = range2 [0.600 - 1.200]',
		11 => 'rating_helpful = range2 [1.500 - 2]',
		12 => 'rating_helpful = range3 [2 - 2.500]',
		13 => 'rating_difficulty = range4 [3.400 - 4.200]',
		14 => 'count_enrolled = range2 [87.400 - 171.800]',
		15 => 'rating_materials = range2 [1.600 - 2.200]',
		16 => 'rating_specifications = range3 [1.200 - 1.800]',
		17 => 'percent_finished = range3 [58.500 - 79.250]',
		18 => 'rating_materials = range1 [-∞ - 1.600]',
		19 => 'rating_work_during_semester = range1 [-∞ - 1.400]',
		20 => 18101,
		21 => 18104,
		22 => 'count_enrolled = range3 [171.800 - 256.200]',
		23 => 'semester = b151',
		24 => 'rating_materials = range3 [2.200 - 2.800]',
		25 => 'percent_finished = range2 [37.750 - 58.500]',
		26 => 18105,
		27 => 'rating_work_during_semester = range5 [2.600 - ∞]',
		28 => 'count_finished = range2 [55.200 - 107.400]',
		29 => 'count_finished = range4 [159.600 - 211.800]',
		30 => 'count_finished = range3 [107.400 - 159.600]',
	];

	public function index() {

		return view('content', ['conclusions' => $this->conclusions]);
    }

	public function fetchRules(Request $request) {

		$support = $request->input('support');
		$confidence = $request->input('confidence');
		$selectedConclusions = $request->input('conclusions');
		if (is_array($selectedConclusions)) $selectedConclusions = array_keys($selectedConclusions);
//		dd(floatval($support), floatval($confidence), $selectedConclusions);


		// TODO simon  HERE
		//		$fileLocation = ;
		$dataFile = file(__DIR__ . '/../../../resources/data/rules_024_80.csv');
		$data = array_map('str_getcsv', $dataFile);

		$coursesFile = file(__DIR__ . '/../../../resources/data/dataset_SMALL_2_discretization_binning.csv');
		$courses = array_map('str_getcsv', $coursesFile);
		$columnNames = $courses[0];
		$courses = array_slice($courses, 1);
//		dd($columnNames, $courses[0]);

		$formattedData = [];
//		$conclusions = [];
		foreach ($data as $rule) {
//			$premise = $this->formatRule($rule[1]);
//			$conclusion = $this->formatRule($rule[2]);
			$premise = $rule[1];
			$conclusion = $rule[2];
			$itemset = $this->getItemset($rule);

//			$conclusionItemset = explode(', ', $conclusion);
//			foreach ($conclusionItemset as $item) {
//				$conclusions[$item] = 1;
//			}

//			dd(floatval($rule[3]), floatval($rule[4]));

			if (floatval($support) > floatval($rule[3]) || floatval($confidence) > floatval($rule[4])) {
				continue;
			}

			$match = empty($selectedConclusions) ? true : false;
			foreach ((array)$selectedConclusions as $selectedConclusion) {
				if (str_contains($conclusion, $selectedConclusion)) {
//					dd($conclusion, $selectedConclusion);
					$match = true;
					break;
				}
			}
			if ($match) {
				$formattedData[] = [
					'premise' => $this->formatRule($premise),
					'conclusion' => $this->formatRule($conclusion),
//					'premise' => $premise,
//					'conclusion' => $conclusion,
					'support' => $rule[3],
					'confidence' => $rule[4],
					'itemset' => $itemset,
					'courses' => $this->getMatchingCourses($columnNames, $courses, $itemset),
				];
			}
		}
//		dd($formattedData);
//	 	var_export(array_keys($conclusions));

		return view('table', ['rules' => $formattedData]);
	}

	/**
	 * @param string $rule
	 */
	private function formatRule($rule) {
		$parts = explode(', ', $rule);
		$result = '';
		foreach ($parts as $part) {
			$baseSpan = '<span class="label label-primary">';
//			$simpleReplaces = [
//				'percent_finished' => $baseSpan . '<span class="glyphicon glyphicon-ok"></span> finished %</span>',
//				'rating_specifications' => $baseSpan . '<span class="glyphicon glyphicon-list-alt"></span> specifications</span>',
//				'count_voted' => $baseSpan . '<span class="glyphicon glyphicon-envelope"></span> voted #</span>',
//				'percent_voted' => $baseSpan . '<span class="glyphicon glyphicon-envelope"></span> voted %</span>',
//				'-∞' => '0',
//				'18101' => '<span class="label label-danger"><span class="glyphicon glyphicon-home"></span> 18101</span>',
//				'18102' => '<span class="label label-danger"><span class="glyphicon glyphicon-home"></span> 18102</span>',
//				'18103' => '<span class="label label-danger"><span class="glyphicon glyphicon-home"></span> 18103</span>',
//				'18104' => '<span class="label label-danger"><span class="glyphicon glyphicon-home"></span> 18104</span>',
//				'18105' => '<span class="label label-danger"><span class="glyphicon glyphicon-home"></span> 18105</span>',
//			];
			$pregReplaces = [
//				'/(.*) = range\d \[(\S+ - \S+)\](.*)/' => '<span class="badge">${1}</span><span class="label label-primary">${2}</span>${3}'
				'/(.*) = range\d \[(\S+ - \S+)\].*/' => '<span class="badge">${1}</span><span class="label label-info">${2}</span>',
				'/(.*) = (.*)/' => '<span class="badge">${1}</span><span class="label label-warning">${2}</span>',
				'/(1810.)/' => '<span class="badge">department</span><span class="label label-success">${1}</span>',
				'/(.+)/' => '<span class="badge">teacher</span><span class="label label-danger	">${1}</span>',
			];

			$formattedRule = $part;
//			foreach ($simpleReplaces as $old =>	$new) {
//				$formattedRule = str_replace($old, $new, $formattedRule);
//			}

			foreach ($pregReplaces as $old => $new) {
				$temp = $formattedRule;
				$formattedRule = preg_replace($old, $new, $formattedRule);
				if ($temp !== $formattedRule) break;
			}

			$result .= ' ' . $formattedRule;
		}
		return $result;
	}

	private function getItemset($rule) {
		$premise = $rule[1];
		$splitPremise = explode(', ', $premise);
		$conclusion = $rule[2];
		$splitConclusion = explode(', ', $conclusion);

		$itemset = array_merge($splitPremise, $splitConclusion);
		return $itemset;
	}

	private function getMatchingCourses(array $columns, array $courses, array $itemset) {
		$indexes = [];
		foreach ($itemset as $item) {
			$indexes[] = array_search($item, $columns);
		}

		$result = [];
		foreach ($courses as $course) {
			$match = true;
			foreach ($indexes as $index) {
				if (strtolower($course[$index]) === 'false') {
					$match = false;
					break;
				}
			}
			if ($match) $result[] = $course[count($course) - 1];
		}
		return $result;
	}

	private function balba() {
		$conclusions = [
			0 => 'rating_specifications = range2 [0.600 - 1.200]',
			1 => 'count_voted = range1 [-∞ - 13]', 2 => 'percent_finished = range1 [-∞ - 37.750]', 3 => 'percent_voted = range1 [-∞ - 25.750]', 4 => 'count_finished = range1 [-∞ - 55.200]', 5 => 18103, 6 => 'rating_specifications = range3 [1.200 - 1.800]', 7 => 'rating_materials = range1 [-∞ - 1.600]', 8 => 'rating_helpful = range3 [2 - 2.500]', 9 => 'percent_finished = range4 [79.250 - ∞]', 10 => 'count_enrolled = range1 [-∞ - 87.400]', 11 => 'rating_difficulty = range4 [3.400 - 4.200]', 12 => 'rating_topic = range1 [-∞ - 1.800]', 13 => 'rating_difficulty = range2 [1.800 - 2.600]', 14 => 'rating_work_during_semester = range3 [1.800 - 2.200]', 15 => 'rating_difficulty = range3 [2.600 - 3.400]', 16 => 'percent_finished = range3 [58.500 - 79.250]', 17 => 'rating_helpful = range1 [-∞ - 1.500]', 18 => 'percent_finished = range2 [37.750 - 58.500]', 19 => 18102, 20 => 'rating_materials = range3 [2.200 - 2.800]', 21 => 'rating_work_during_semester = range4 [2.200 - 2.600]', 22 => 'rating_materials = range2 [1.600 - 2.200]', 23 => 'rating_work_during_semester = range1 [-∞ - 1.400]', 24 => 'rating_work_during_semester = range5 [2.600 - ∞]', 25 => 18101, 26 => 'rating_work_during_semester = range2 [1.400 - 1.800]', 27 => 'count_finished = range3 [107.400 - 159.600]', 28 => 'rating_helpful = range2 [1.500 - 2]', 29 => 'semester = b151', 30 => 'count_voted = range2 [13 - 25]', 31 => 'semester = b142', 32 => 'rating_specifications = range4 [1.800 - 2.400]', 33 => 18104, 34 => 'count_enrolled = range2 [87.400 - 171.800]', 35 => 'rating_topic = range3 [2.600 - 3.400]', 36 => 'rating_topic = range2 [1.800 - 2.600]', 37 => 'count_finished = range2 [55.200 - 107.400]', 38 => 'Richtr Radek Ing.', 39 => 'Moucha Alexandru Ing. Ph.D.', 40 => 'Petr Ivo Ing.', 41 => 'Bařinka Lukáš Ing.', 42 => 18105, 43 => 'rating_materials = range4 [2.800 - 3.400]', 44 => 'percent_voted = range2 [25.750 - 50.500]', 45 => 'rating_helpful = range4 [2.500 - 3]', 46 => 'count_finished = range4 [159.600 - 211.800]', 47 => 'count_enrolled = range3 [171.800 - 256.200]', 48 => 'rating_topic = range4 [3.400 - 4.200]', 49 => 'count_voted = range3 [25 - 37]', 50 => 'rating_difficulty = range1 [-∞ - 1.800]', 51 => 'count_voted = range4 [37 - 49]', 52 => 'rating_specifications = range5 [2.400 - ∞]', 53 => 'count_finished = range6 [264 - 316.200]', 54 => 'count_finished = range5 [211.800 - 264]', 55 => 'count_enrolled = range5 [340.600 - 425]', 56 => 'Vašata Daniel Ing.', 57 => 'Valentová Kateřina PhDr.', 58 => 'Scholtzová Jiřina RNDr. Ph.D.', 59 => 'Polách Radomír Ing.', 60 => 'Jirkovský Vojtěch Ing.', 61 => 'Guth Ondřej Ing. Ph.D.', ];
	}
}
