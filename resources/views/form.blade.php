{{--<form class="form-horizontal">--}}
	{{--<div class="form-group float-input">--}}
		{{--<label for="support" class="col-sm-2 control-label">Support</label>--}}
		{{--<div class="col-sm-2">--}}
			{{--<input type="number" min="0" max="1" step="any" class="form-control" id="support" value="0.3">--}}
		{{--</div>--}}
	{{--</div>--}}
	{{--<div class="form-group float-input">--}}
		{{--<label for="support" class="col-sm-2 control-label">Support</label>--}}
		{{--<div class="col-sm-2">--}}
			{{--<input type="number" class="form-control" id="support" value="0.3">--}}
		{{--</div>--}}
	{{--</div>--}}
	{{--<div class="form-group">--}}
		{{--<label for="inputPassword3" class="col-sm-2 control-label">Password</label>--}}
		{{--<div class="col-sm-10">--}}
			{{--<input type="password" class="form-control" id="inputPassword3" placeholder="Password">--}}
		{{--</div>--}}
	{{--</div>--}}
{{--</form>--}}
<hr>
<form class="form-inline support-confidence-form">
	<div class="form-group  col-sm-offset-3 support-form-group">
		{{--<label for="support">Support (0.02 - 1): </label>--}}
		<div class="input-group">
			<div class="input-group-addon">Support</div>
			<input type="number" min="0.02" max="1" step="0.1" class="form-control" id="support" value="0.3">
		</div>
	</div>
	<div class="form-group col-sm-offset-2 confidence-form-group">
		{{--<label for="confidence">Confidence (0.8 - 1): </label>--}}
		<div class="input-group">
			<div class="input-group-addon">Confidence</div>
			<input type="number" min="0.8" max="1" step="0.01" class="form-control" id="confidence" value="0.8">
		</div>
	</div>
</form>
<hr>
<form>
	<div class="clearfix">
	@foreach($conclusions as $conclusion)
		<div class="form-group conclusion-checkbox">
			<div class="col-sm-offset-1 col-sm-10">
				<div class="checkbox">
					<label>
						<input type="checkbox" value="{{ $conclusion }}"> {{ $conclusion }}
					</label>
				</div>
			</div>
		</div>
	@endforeach
	</div>
	{{--<div class="form-group">--}}
		{{--<div class="col-sm-offset-2 col-sm-10">--}}
			{{--<button type="submit" class="btn btn-default">Sign in</button>--}}
		{{--</div>--}}
	{{--</div>--}}
</form>
<hr>