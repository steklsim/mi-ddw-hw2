<table class="table table-striped table-bordered clearfix" id="rules-table">
	<tr>
		<th>Premise</th>
		<th>Conclusion</th>
		<th>Support</th>
		<th>Confidence</th>
	</tr>
	@foreach($rules as $i => $rule)
		<tr data-toggle="collapse" data-target="#accordion{{ $i }}" class="clickable">
			<td>{!! $rule['premise'] !!}</td>
			<td>{!! $rule['conclusion'] !!}</td>
			<td>{{ number_format($rule['support'], 3) }}</td>
			<td>{{ number_format($rule['confidence'], 3) }}</td>
		</tr>
		<tr>
			<td colspan="4" class="courses-cell">
				<div id="accordion{{ $i }}" class="courses collapse">
					{{--<ul>--}}
					@foreach($rule['courses'] as $course)
						<p class="text-primary course">{{ $course }}</p>
					@endforeach
					{{--</ul>--}}
				</div>
			</td>
		</tr>
	@endforeach
</table>