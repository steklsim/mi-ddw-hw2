/**
 * Created by simon on 18.5.16.
 */

var updateTable = function(data) {
	console.log(data);
	var tableDiv = $('#rules');
	tableDiv.empty();
	tableDiv.append(data);
};

var getData = function () {
	var inputs = {
		support: $('#support').val(),
		confidence: $('#confidence').val(),
		conclusions: {}
	};
	$('input[type=checkbox]:checked').each(function(_, input) {
		inputs.conclusions[$(input).val()] = 1;
	});
	console.log(inputs);
	$.ajax({
		method: 'POST',
		url: 'http://localhost:8001/fetchRules',
		data: inputs
	}).done(function(data) {
		updateTable(data);
	});
};

$(document).ready(getData());

$('input').change(function(e) {
	getData();
});